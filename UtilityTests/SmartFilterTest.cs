﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utility;

namespace UtilityTests
{
    [TestClass]
    public class SmartFilterTest
    {
        private readonly SmartSubstringFinder _smart = new SmartSubstringFinder();

        [TestMethod]
        public void IfTextIsNullOrEmpty_ReturnFalse()
        {
            Assert.IsFalse(_smart.Find(null, null));
            Assert.IsFalse(_smart.Find("", null));
            Assert.IsFalse(_smart.Find("", ""));
        }

        [TestMethod]
        public void IfPartialTextIsNullOrEmpty_ReturnTrue()
        {
            Assert.IsTrue(_smart.Find(null, "abc"));
            Assert.IsTrue(_smart.Find("", "abc"));
        }

        [TestMethod]
        public void IfNoLetterMatches_ReturnFalse()
        {
            Assert.IsFalse(_smart.Find("d", "abc"));
        }

        [TestMethod]
        public void IfOneLetterMatches_ReturnTrue()
        {
            Assert.IsTrue(_smart.Find("a", "abc"));
            Assert.IsTrue(_smart.Find("b", "abc"));
            Assert.IsTrue(_smart.Find("c", "abc"));
        }

        [TestMethod]
        public void IfAdjacentLettersMatch_ReturnTrue()
        {
            Assert.IsTrue(_smart.Find("ab", "abc"));
            Assert.IsTrue(_smart.Find("bc", "abc"));
        }

        [TestMethod]
        public void IfNonAdjacentLettersMatch_ReturnTrue()
        {
            Assert.IsTrue(_smart.Find("ac", "abc"));
        }

        [TestMethod]
        public void IfMatchingLettersAreSeparatedByNonMatching_ReturnFalse()
        {
            Assert.IsFalse(_smart.Find("adc", "abc"));
        }

        [TestMethod]
        public void IgnoreCaseSensitivity()
        {
            Assert.IsTrue(_smart.Find("Ac", "abC"));
        }

        [TestMethod]
        public void ShouldPassExamples()
        {
            Assert.IsTrue(_smart.Find("dhch", "derhirsch"));
            Assert.IsTrue(_smart.Find("113tb", "D113.T_P4B2X"));
            Assert.IsTrue(_smart.Find("._x", "D113.T_P4B2X"));
            Assert.IsTrue(_smart.Find("Visualstudio cftswas",
                "Visual Studios AutoCompletion functions the same way as this."));
        }

        [TestMethod]
        public void ShouldFailExamples()
        {
            Assert.IsFalse(_smart.Find("dihch", "derhirsch"));
            Assert.IsFalse(_smart.Find("wx ", "wwwwwwwwwwx")); //Notice the whitespace after wx ;)
            Assert.IsFalse(_smart.Find("2222892", "22892"));
        }
    }
}
