using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utility;

namespace UtilityTests
{
    [TestClass]
    public class SmartCollectionFilterTest
    {
        private readonly SmartSubstringFinder _smart = new SmartSubstringFinder();

        [TestMethod]
        public void IfNullOrEmptyList_ReturnEmptyList()
        {
            Assert.AreEqual(0, _smart.FilterCollection("", Null()).Count());
            Assert.AreEqual(0, _smart.FilterCollection("", new List<string>()).Count());
        }

        private static List<string> Null()
        {
            return null;
        }

        [TestMethod]
        public void IfNullOrEmptyPartialText_ReturnList()
        {
            var list = new List<string>{"String 1", "String 2"};

            Assert.AreEqual(list, _smart.FilterCollection(null, list));
            Assert.AreEqual(list, _smart.FilterCollection("", list));
        }

        [TestMethod]
        public void ReturnListWherePartialStringWasFound()
        {
            var list = new List<string> { "abc", "cde", "bcd" };
            var result = _smart.FilterCollection("bc", list).ToList();

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(list[0], result[0]);
            Assert.AreEqual(list[2], result[1]);
        }

        [TestMethod]
        public void IfCollectionIsDictionary_FilterKeyAndValueAsOneStringWhitespaceSeparated()
        {
            var hur = new Dictionary<string, string>
            {
                {"4840", "D114.T_P4B2"},
                {"4842", "D114.T_P4A2"},
                {"13564", "D113.T_P4B2X"}
            };

            var result = _smart.FilterCollection("48 A", hur).ToList();
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("4842", result.Single().Key);
            Assert.AreEqual("D114.T_P4A2", result.Single().Value);
        }
    }
}