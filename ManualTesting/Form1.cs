﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace ManualTesting
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();


            filterableList1.SetSelection(new Dictionary<string, string>
            {
                {"4840", "D114.T_P4B2" },
                {"4842", "D114.T_P4A2" },
                {"13564", "D113.T_P4B2X" }
            });
            filterableList1.SelectionConfirmed += FilterableList1OnSelectionConfirmed;
        }

        private static void FilterableList1OnSelectionConfirmed(object sender, KeyValuePair<string, string> keyValuePair)
        {
            MessageBox.Show("Key: " + keyValuePair.Key + " Value: " + keyValuePair.Value);
        }
    }
}
