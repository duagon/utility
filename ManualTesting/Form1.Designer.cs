﻿using System.Collections.Generic;
using Utility;

namespace ManualTesting
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // filterableList1
            // 
            filterableList1 = new FilterableSelection();
            this.filterableList1.Location = new System.Drawing.Point(0, 0);
            this.filterableList1.Name = "filterableList1";
            this.filterableList1.Size = new System.Drawing.Size(439, 408);
            this.filterableList1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 408);
            this.Controls.Add(this.filterableList1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private Utility.FilterableSelection filterableList1;
    }
}

