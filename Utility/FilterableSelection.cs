﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Utility
{
    public partial class FilterableSelection : UserControl, IFilterableSelection
    {
        private Dictionary<string, string> _originalSelection;
        private readonly SmartSubstringFinder _smart = new SmartSubstringFinder();

        public event EventHandler<KeyValuePair<string, string>> SelectionConfirmed;

        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);

        public FilterableSelection()
        {
            InitializeComponent();
            SetTextFieldDescription();
        }

        private void SetTextFieldDescription()
        {
            SendMessage(textBox1.Handle, 0x1501, 0, "Filter");
        }

        public void SetSelection(Dictionary<string, string> selection)
        {
            _originalSelection = selection;
            SetupListView();
        }

        private void SetupListView()
        {
            listView1.SetObjects(_smart.FilterCollection(textBox1.Text, _originalSelection));
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            SetupListView();
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            ConfirmSelection((KeyValuePair<string, string>)listView1.FocusedObject);
        }

        private void ConfirmSelection(KeyValuePair<string, string> selected)
        {
            SelectionConfirmed?.Invoke(this, selected);
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (IsEnterKeyDown(e))
                ConfirmSelectionIfOnlyOneIsLeftAfterFiltering();
        }

        private void ConfirmSelectionIfOnlyOneIsLeftAfterFiltering()
        {
            var filterCollection = _smart.FilterCollection(textBox1.Text, _originalSelection).ToList();
            if (filterCollection.Count == 1)
                ConfirmSelection(filterCollection.First());
        }

        private static bool IsEnterKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return false;
            e.SuppressKeyPress = true;
            return true;
        }
    }
}
