﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utility
{
    public class SmartSubstringFinder
    {
        public bool Find(string partialText, string text)
        {
            if (string.IsNullOrEmpty(text))
                return false;

            if (string.IsNullOrEmpty(partialText))
                return true;

            for (int i = 0, p = 0; i <= text.Length && p < partialText.Length; i++, p++)
            {
                i = text.IndexOf(partialText[p].ToString(), i, StringComparison.InvariantCultureIgnoreCase);
                if (-1 == i) return false;
            }

            return true;
        }

        public IEnumerable<string> FilterCollection(string partialText, List<string> textCollection)
        {
            if(textCollection == null || !textCollection.Any())
                return new List<string>();

            if(string.IsNullOrEmpty(partialText))
                return textCollection;

            return textCollection.Where(c => Find(partialText, c));
        }

        public IEnumerable<KeyValuePair<string, string>> FilterCollection(string partialText, Dictionary<string, string> textCollection)
        {
            if (textCollection == null || !textCollection.Any())    
                return new Dictionary<string, string>();

            if (string.IsNullOrEmpty(partialText))
                return textCollection;

            return textCollection.Where(d => Find(partialText, d.Key + " " + d.Value));
        }
    }
}
