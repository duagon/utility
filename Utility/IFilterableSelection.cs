﻿using System;
using System.Collections.Generic;

namespace Utility
{
    public interface IFilterableSelection
    {
        event EventHandler<KeyValuePair<string, string>> SelectionConfirmed;
        void SetSelection(Dictionary<string, string> selection);
    }
}